package dk.payment.services.gateways;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import dk.payment.services.entities.User;
import dk.payment.services.user.UserGateway;

public class UserGatewayImplTest {
	UserGateway userGateway;
	
	@Before
	public void before() {
		userGateway = new UserGatewayImpl();
	}
	
	@Test
	public void givenAddingUserToDatabase_returnCreatedUser() {
		//arrange
		User user = new User();
		user.setEmail("kaloyan@penov.com");
		//act
//		User returnedUser =  userGateway.addUser(user);
		userGateway.addUser(user);
		User returnedUser = userGateway.getUser("kaloyan@penov.com");
		//assert
		assertEquals(returnedUser.getEmail(), user.getEmail());
	}
}
